<?php

include 'functions.php';


session_start();
if (isset($_SESSION['settings'])) $_SESSION['settings'] = array();
$_SESSION['debug'] = true;
$_SESSION['settings']['mode'] = 'render';


echo "<style>body {white-space: pre;}</style>";

/* this needs a separate page or js hack
https://stackoverflow.com/questions/2317508/get-fragment-value-after-hash-from-a-url
if( strpos( $url, "#" ) === true ) {
	echo "found";
	$anchor = explode( "#", $url )[1];
	foreach (array('edit', 'settings') as $opt) {
		if (!strcmp($anchor, $opt)) $_SESSION['settings']['mode'] = $opt;
	}
	
} */

$sections = get_sections();
foreach ($sections as $section) {
	if (key_exists('sub', $section)) {
		$subparts = $section['sub'];
		foreach ($subparts as $subpart) {
			$part = get_person($subpart);
			echo "<div class='$subpart'>$part</div>";
		}
	}
}





include 'footer.php';