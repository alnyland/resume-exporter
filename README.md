# Resume organizer/renderer
Emphasis on the renderer part for now, and not keeping track of all relevant data for now. Just on resume output data and segmenting for different purpose resumes.

In addition, easy alteration of theme (visual appearance) and job entry info.

## Project structure
Folders such as `data`, `samples`, and `settings`, etc, should be stored into a database when it makes sense to (and thus these are removed).

## Development notes
For settings, file structure type is designated via:
 - `+` designates ordered file type, this one isn't fully defined yet
 - `=` designates named data, delimiter is `:`
