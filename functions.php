<?php

// helper functions
function parse_file($file) {
	$retval = array();
	while (($line = fgets($file)) !== false) {
		if (!strcmp($line[0], '=') || !strcmp($line[0], '+')) {
			if (!strcmp($line[0], '=')) $filetype = 'named';
			else $filetype = 'ordered';
			continue; // skip first line
		}
		if (!strcmp($filetype, 'ordered')) {
			if (!strcmp($line[0], '#')) continue; // comments, likely not needed
			if (!strcmp($line[0], ' ')) {	// TODO fix these to be tabs if this method is used
				if (!key_exists('sub', $retval[count($retval)-1])) $retval[count($retval)-1]['sub'] = array();
				$retval[count($retval)-1]['sub'][] = $line;
			} else {
				$retval[] = array('content' => $line);
			}
		}
		if (!strcmp($filetype, 'named')) {
			$parts = explode(':', $line);
			$retval[$parts[0]] = trim($parts[1]);
		}
    }
	return $retval;
}

function get_sections() {
	$file = @fopen('settings_resumeorder.txt', 'r');
	return parse_file($file);
}

function get_person($part) {
	$part = trim(strtolower($part));
	$file = @fopen('person.txt', 'r');
	return parse_file($file)[$part];
}